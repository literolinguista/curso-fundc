#include <stdio.h>
#include <locale.h>

int main() {
	printf("Minha localização é %s.\n", setlocale(LC_ALL, NULL));
	printf("Olá, mundo!\n\n");
	return 0;

	printf("Minha localização é %s.\n", setlocale(LC_ALL, ""));
	printf("Olá, mundo!\n\n");
	return 0;
}
